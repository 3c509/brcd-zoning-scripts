#!/usr/bin/perl


open FP, "alias.dat" or die;
chomp (@array = (<FP>));
close FP;

foreach $line (@array) {
  $line =~ s/^\s+//;
  $line =~ s/\s+$//;
  $alias = (split /\s+/, $line)[0];
  $wwn = (split /\s+/, $line)[1];
  print "alicreate \"" . $alias . "\", \"" . $wwn . "\"\n";
}

